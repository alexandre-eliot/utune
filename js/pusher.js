$(document).ready(function () {

  $('.button-cue').click(function () {
    if($(this).hasClass('active')){
      $(this).removeClass('active');
      $('aside').animate({width: "0px"},
        {duration:300,
        complete: function(){
          $(this).hide();
        }
      });
      // $('aside').hide();
    }
    else{
      $(this).addClass('active');
      $('aside').show();
      // $('aside').removeAttr('style');
      $('aside').animate({}, //width:"300px"
        {duration:300,
        complete: function(){
          $(this).removeAttr('style');
        }
      });
    }
  });
});