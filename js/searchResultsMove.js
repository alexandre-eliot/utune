$(document).ready(function(){searchResultsMove();});
$(document).change(function(){searchResultsMove();});

function searchResultsMove() {

	$(document).mouseup(function (e){

		var container = $('#main-wrapper #search-results');

		if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.removeAttr("style");
		}
	});
}

function showSearchResBar() {
	$('#main-wrapper #search-results').css("left","0");
}
