$('#myForm').submit(SubForm(e));

function SubForm(e){
    e.preventDefault();
    var url=$(this).closest('form').attr('action'),
        data=$(this).closest('form').serialize();
    $.ajax({
        url:url,
        type:'post',
        data:data,
        success:function(){
           //whatever you wanna do after the form is successfully submitted
           
        }
    });
}
////////////////          OR          ////////////////

function SubForm (){
    $.ajax({
        url:'/Person/Edit/@Model.Id/',
        type:'post',
        data:$('#myForm').serialize(),
        success:function(){
            alert("worked");
        }
    });
}