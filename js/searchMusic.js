$('#filter-search').on('keyup', searchMusic);
$('.filter-form select[name="genre"]').on('change', searchMusic);
$('.filter-form select[name="artist"]').on('change', searchMusic);
$('.filter-form').on('submit', searchMusic);

function searchMusic(){

	var title = $('#filter-search').val();
	title= title.replace(/%20/g, "+");
	console.log('title : '+title);

	var genre = $('.filter-form select[name="genre"]').val();
	var artistName = $('.filter-form select[name="artist"]').val();
	var albumName = "null";
	if ($('.album[selected="selected"]').length){
		albumName = $('.album[selected="selected"]').attr("data-album");
	}

	$.ajax({
		type: "GET",
		url: 'modele/searchMusic.php',
		data: {title: title, genre: genre, artistName: artistName, albumName: albumName},
		dataType: 'json',
		success : function(data) {
			dispMusics(data);
		},
		error : function (xhr, ajaxOptions, thrownError) {
			// console.log(xhr.responseText);
			$('#musics-container').html('<span>'+xhr.responseText+'</span>');
		}
	});

	// ajaxJson({method : "GET", url : "modele/getArtist"});

	return false; // prevents from page reload
}

function dispMusics(musics) {

	$('#musics-container').empty();
	if (!musics.length) {
		$('#musics-container').html('<span>No results found</span>');
	} else {
		$.get("tpl/musicItem.php", function(data) {
			var music;
			var elt='';
			for (var i = 0; i < musics.length; i++) {
				music = musics[i];
				elt = tplawesome(data, [{"title":music.title, "videoid":music.videoId, "cover":music.cover, "artist":music.artist}]);
				$('#musics-container').append(elt);
			};
			$("#musics-container ul li .addFav").click(function() {
				addFav($(this).closest('li').attr('data-video-id'));
			});
		});
	}
}

function setSelectArtist (data) {
	$('select[name="artist"]').empty();
	$('select[name="artist"]').append('<option value="null">Artist</option>');
	if (data.length>0) {
		for (var i = 0; i < data.length; i++) {
			var elt='<option>'+data[i].artist+'</option>';
			$('select[name="artist"]').append(elt);
		}
	}
}


function dispAlbumFilter () {
	var elt;
	elt='<div class="filter-item -album">';
	elt+='<span>Album : '+$('.album[selected="selected"]').attr('data-album')+'</span>';
	elt+='<a>'
	elt+='<span class="rem"></span>'
	elt+='</a>'
	elt+='</div>';
	$('.filter-items').append(elt);

	$('.filter-item.-album > a').on('click', remAlbumFilter);
};

function remAlbumFilter() {
	$('.album[selected="selected"]').removeAttr('selected');
	$('.filter-items .filter-item.-album').remove();
	searchMusic();
}

$('.albums .album > img').on('click', albumFilter);

function albumFilter () {
	$('.filter-items .filter-item.-album').remove();
	$('.album[selected="selected"]').removeAttr('selected');
	$(this).closest('.album').attr('selected','selected');
	searchMusic();
	dispAlbumFilter();
}

function albumChange (data) {
	var title = $('#filter-search').val();
	title= title.replace(/%20/g, "+");
	var genre = $('.filter-form select[name="genre"]').val();
	var artistName = $('.filter-form select[name="artist"]').val();
	var albumName = "null";
	$.ajax({
		type: "GET",
		url: 'modele/searchMusic.php',
		data: {title: title, genre: genre, artistName: artistName, albumName: albumName},
		dataType: 'json',
		success : function(data) {
			$('.albums').empty();
			if (!data.length) {
				$('.albums').html('<span>No results found</span>');
			} else {
				var fl='';
				for (var i=0; i < data.length ; i++) {
					if(data[i]['album'][0]!=fl){
						fl=data[i]['album'][0];
					}
					var elt='<div>';
					elt+='<span>'+fl+'</span>';
					elt+='<div class="album" data-album="'+data[i]['album']+'">';
					elt+='<img src="'+data[i]['cover']+'">';
					elt+='<legend>'+data[i]['album']+'</legend>';
					elt+='</div>';
					elt+='</div>';
					$('.albums').append(elt);
				}
			}
		},
		error : function (xhr, ajaxOptions, thrownError) {
			// console.log(xhr.responseText);
		}
	});
}