function removeFav(videoId) {
	$.ajax({
		type: "GET",
		url: 'modele/removeFav.php',
		data: {videoId: videoId},
		dataType: 'json',
		success : function(data) {
			searchFav();
		},
		error : function (xhr, ajaxOptions, thrownError) {
			// console.log(xhr.responseText);
			$('#favorites-container').html('<span>'+xhr.responseText+'</span>');
		}
	});
}

function addFav(videoId) {
	$.ajax({
		type: "GET",
		url: 'modele/addFav.php',
		data: {videoId: videoId},
		dataType: 'json',
		success : function(data) {
			searchFav();
		},
		error : function (xhr, ajaxOptions, thrownError) {
			// console.log(xhr.responseText);
			$('#favorites-container').html('<span>'+xhr.responseText+'</span>');
		}
	});
}
