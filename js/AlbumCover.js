$('#artist').on('keyup', getAlbumCover);
$('#album').on('keyup', getAlbumCover);

function getAlbumCover(){
	
	var query = '';
	query+=$('#album').val();
	query+=' ';
	query+=$('#artist').val();
	query=encodeURIComponent(query).replace(/%20/g, "+");

	var entity = 'album';
	var country = 'us';

	$.ajax({
		type: "GET",
		crossDomain: true,
		url: 'modele/albumCoverApi.php',
		data: {query: query, entity: entity, country: country},
		dataType: 'json'
	}).done(function(data) {
		if (data.error) {
			$('#imgCover').alt=data.error;
		} else {
			if (!data.length) {
				$('#imgCover').alt='No result found';
			} else {
				$('#cover').val(data[0].url);
				$('#imgCover').attr('src',data[0].url);
				$('#imgCover').attr('alt',data[0].title);
			}
		}
	});
}