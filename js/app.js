function tplawesome(e,t){res=e;for(var n=0;n<t.length;n++){res=res.replace(/\{\{(.*?)\}\}/g,function(e,r){return t[n][r]})}return res}

$(document).on('ready change',function(){
    $(".play").click(function(){
        changeVideo($(this).closest('.music').attr('data-video-id'));
    });
    $('.addFav').click(function(){
        addToFavorites($(this).closest('li').attr('data-video-id'));
    });
    $('.addLib').click(function(){
        var title = $(this).closest('li')[0].getElementsByClassName('music-title')[0].innerHTML;
        var videoId = $(this).closest('li').attr('data-video-id');
        addToLibrary(title, videoId);
    });
});

$($('#search').on('keyup',searchVideos));
$($('#search_form').on('submit', searchVideos));

function searchVideos(event) {
    event.preventDefault();

    // prepare the request
    var request = gapi.client.youtube.search.list({
        part: "snippet",
        type: "video",
        q: encodeURIComponent($("#search").val()).replace(/%20/g, "+"),
        maxResults: 20,
        // regioncode: 'FR',
        order: "viewCount",
        publishedAfter: "2015-01-01T00:00:00Z"
    });
    // execute the request
    request.execute(function(response) {
        var results = response.result;
        $("#search-results ul").empty();
        $.each(results.items, function(index, item) {
            $.get("tpl/musicItem.php", function(data) {
                var elt = tplawesome(data, [{"title":item.snippet.title, "videoid":item.id.videoId}]);
                $("#search-results ul").append(elt);
                $("#search-results ul li:last-of-type .play").click(function(){
                    changeVideo($(this).attr('data-video-id'));
                });
            });
        });
        /*resetVideoHeight();*/
    });
    $("#search-results ul").append('<script src="js/jquery.sortable.js"></script>');
    $(".sortable").sortable({items : 'li'});
    $(".sortable").sortable({
        connectWith: '.connected'
    });
    return false;
}

/*$(window).on("resize", resetVideoHeight);*/


/*function resetVideoHeight() {
    $(".video").css("height", $("#search-results table tbody").width() * 9/16);
}*/

function changeVideo(id) {
    player.loadVideoById(
        id,
        0,
        'small'
        );
}

function init() {
    gapi.client.setApiKey("AIzaSyBt0oCPNIzWcqijk0unGI6tbcIjmab3Du4");
    gapi.client.load("youtube", "v3", function() {
        // yt api is ready
    });
}

function addToFavorites(videoId){
    $.ajax({
        type: "POST",
        url: 'modele/addToFavorites.php',
        data: { },
        success: function(data){
        }
    });
}

function addToLibrary(title,videoId){
    $.ajax({
       type: "POST",
       url: 'add_music/view/index.php',
       data: {video_id : videoId, title:title},
       success: function(data){
           $('#main-wrapper main').html(data);
       }
   });
}

