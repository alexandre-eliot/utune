
// $(window).bind('popstate', function() {
// 	if (location.pathname.substr(location.pathname.lastIndexOf("/")+1).toLowerCase()!=''){
// 		var url = location.pathname+'/view/index.php';
// 		$.get(url, function(data) {
// 			$('#main-wrapper main').html(data);
// 		});
// 		var Aelt = $('header a[href="'+location.pathname.substr(location.pathname.lastIndexOf("/")+1)+'"]');
// 		Aelt.attr("selected","selected");
// 		var NAelt = $('header a:not([href="'+location.pathname.substr(location.pathname.lastIndexOf("/")+1)+'"])');
// 		NAelt.removeAttr("selected");
// 	}
// });

document.addEventListener("DOMContentLoaded", function(event) {

	var li = $("header a");

	li.click(function() {
		history.pushState({ path: this.path }, '', this.href);
		li.each(function(i){
			li[i].removeAttribute("selected");
		});
		$(this).attr("selected","selected");
		var regPage = '/?page=/';
		var url = regPage.exec(this.href);
		console.log(url);
		// $.ajax({
		// 	type: "POST",
		// 	url: url,
		// 	data: { },
		// 	success: function(data){
		// 		$('#main-wrapper main').html(data);
		// 	}
		// });
		// C921F44D7D642FDE1E1E7F75F1
		$.get(url, function(data) {
			$('#main-wrapper main').html(data);    
		});
		return false;
	});

	$('#logout').click(function() {
		var url = '/Utune-3/';
		$.ajax({
			type: "POST",
			url: url,
			data: { logout : true},
			success: function(data){
				window.location.href = url;
			}
		}); 
	});

});
