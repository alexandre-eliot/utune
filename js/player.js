var tag = document.createElement('script');

tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        width: '0',
        height: '0',
        videoId: '',
        playerVars: {
            'controls' : 0,
            'modestbranding' : 1,
            'rel' : 0,
            'showinfo' : 0
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// function changeVideoById(id){
//     stopVideo();
//     clearTimeout(sliderBarTimer);
//     player.cueVideoById(
//         id,
//         0,
//         'small'
//         );
//     player.loadVideoById(
//         id,
//         0,
//         'small'
//         );
//     playVideo();
// }

function playVideo(id) {
    player.loadVideoById(
        id,
        0,
        'small'
        );
}

var ready = false;
function onPlayerReady(event) {
    ready = true;

    event.target.setPlaybackQuality('small');
    player.setVolume(50);

    var vD = displayTime(player.getDuration());
    $('.video-duration').text(' / '+vD);

    $('.button-play').add('.button-pause').click(function() {

        var pst = player.getPlayerState();
        switch(pst){
            case 0:
            case 2:
            case 5:
            playVideo();
            $(this).removeClass('glyphicon-pause');
            $(this).addClass('glyphicon-play');
            break;
            case 1:
            pauseVideo();
            $(this).removeClass('glyphicon-play');
            $(this).addClass('glyphicon-pause');
            break;
        }
    });

    $('.button-loop').click(function() {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            player.setLoop(true);
        }
        else{
            $(this).addClass('active');
            player.setLoop(false);
        }
    });

    $('.button-rand').click(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            player.setShuffle(true);
        }
        else{
            $(this).addClass('active');
            player.setShuffle(false);
        }
    });

    $('.button-prev').click(function(){
        player.previousVideo();
    });

    $('.button-next').click(function(){
        player.nextVideo();
    });

    // var query = document.querySelector.bind(document);

    // Once the user clicks a custom fullscreen button
    /*query('#fullsize').addEventListener('click', function(){
        // Play video and go fullscreen
        playVideo();

        var playerElement = query("#player");
        var requestFullScreen = playerElement.requestFullScreen || playerElement.mozRequestFullScreen || playerElement.webkitRequestFullScreen;
        if (requestFullScreen) {
            requestFullScreen.bind(playerElement)();
        }
    });*/
$('#fullsize').on('click', function(){
    if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('#playerFullscreen').remove();
    }
    else{
        $(this).addClass('active');
        var tag = $('<link rel="stylesheet" type="text/css" href="styles/player-fullscreen.css" id="playerFullscreen">')
        var lastLinkTag = $('link[rel="stylesheet"]').last()
        tag.insertAfter(lastLinkTag);
    }
});

    //volume bar event
    var volumeDrag = false;
    $('.volume').on('mousedown', function (e) {
        volumeDrag = true;
        player.unMute();
        changeVolume(e.pageX);
    });
    $(document).on('mouseup', function (e) {
        if (volumeDrag) {
            volumeDrag = false;
            changeVolume(e.pageX);
        }
    });
    $(document).on('mousemove', function (e) {
        if (volumeDrag) {
            changeVolume(e.pageX);
        }
    });

    //sliderbar event
    var timeDrag = false;
    $('.slider').on('mousedown', function (e) {
        timeDrag = true;
        changeTime(e.pageX);
    });
    $(document).on('mouseup', function (e) {
        if (timeDrag) {
            timeDrag = false;
            changeTime(e.pageX);
        }
    });
    $(document).on('mousemove', function (e) {
        if (timeDrag) {
            changeTime(e.pageX);
        }
    });

}

function changeVolume(x) {
    var volume = $('.volume');
    var percentage;
    
    var position = x - volume.offset().left;
    percentage = 100 * position / volume.width();
    

    if (percentage > 100) {
        percentage = 100;
    }
    if (percentage < 0) {
        percentage = 0;
    }

    //update volume bar and video volume
    $('.volumeBar').css('width', percentage + '%');
    player.setVolume(percentage);

};

function changeTime(x){
    player.pauseVideo();
    
    var slider = $('.slider');
    var percentage;

    var position = x - slider.offset().left;
    percentage = 100 * position / slider.width();

    if (percentage > 100) {
        percentage = 100;
    }
    if (percentage < 0) {
        percentage = 0;
    }
    $('.sliderBar').css('width', percentage + '%');

    var timeVideo = player.getDuration();
    var timeSet = timeVideo * (percentage / 100);
    player.seekTo(timeSet);

    playVideo();
}

var sliderBarTimer;
function onPlayerStateChange(event) {

    var vD = displayTime(player.getDuration());
    $('.video-duration').text(' / '+vD);

    if(event.data == YT.PlayerState.BUFFERING){
        var timer = setInterval(function() {
            var loadedFraction = player.getVideoLoadedFraction()*100;
            $('.chargingBar').css('width', loadedFraction+'%');
        }, 100);
    }
    if (event.data == YT.PlayerState.PLAYING) {

        $('.button-play').hide();
        $('.button-pause').show();
        $('[data-video-id="'+player.getVideoData().video_id+'"]').each(function(){
            $(this).find('.play').removeClass('glyphicon-play');
            $(this).find('.play').addClass('glyphicon-pause');
        });

        var timeVideo = player.getDuration();
        sliderBarTimer = setInterval(function() {

            // var volume = player.getVolume();
            // $('.volumeBar').css('width', volume+'%');

            var timeElapsed = player.getCurrentTime();
            var t = displayTime(timeElapsed);
            $('.time-elapsed').text(t);

            var currentTime = ( timeElapsed / timeVideo ) * 100;
            $('.sliderBar').css('width', currentTime+'%');

        }, 100);

    } else {
        clearTimeout(sliderBarTimer);
        $('.button-pause').hide();
        $('.button-play').show();
        $('[data-video-id="'+player.getVideoData().video_id+'"]').each(function(){
            $(this).find('.play').removeClass('glyphicon-pause');
            $(this).find('.play').addClass('glyphicon-play');
        });
    }
}

function displayTime(time){
    var re='';
    var s = Math.floor(time);
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    if(h!=0){
        re = h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s);
    }
    else{
        re = m+":"+(s < 10 ? '0'+s : s);
    }
    return re;
}

function stopVideo() {
    player.stopVideo();
}
function pauseVideo() {
    player.pauseVideo();
}
function playVideo() {
    if(ready) player.playVideo();
    else setTimeout(function(){ playVideo() },1000);
}

function youtubeParser(url){
    var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match&&match[1].length==11)? match[1] : false;
}