$('#filter-search').on('keyup', searchFav);
$('#filter-order > input').on('change', searchFav);
$('.filter-form').on('submit', searchFav);

function searchFav () {

	var filter = $('#filter-search').val();
	filter= filter.replace(/%20/g, "+");

	var order = $('#filter-order > input:checked').val();
	
	$.ajax({
		type: "POST",
		url: 'modele/searchFav.php',
		data: {filter: filter, order: order},
		dataType: 'json',
		success : function(data) {
			dispMusics(data);
		},
		error : function (xhr, ajaxOptions, thrownError) {
			// console.log(xhr.responseText);
			$('#fav-container').html('<span>'+xhr.responseText+'</span>');
		}
	});

	// ajaxJson({method : "GET", url : "modele/getArtist"});

	return false; // prevents from page reload
}
function dispMusics(musics) {

	$('#fav-container').empty();
	if (musics.length){
		$.get("tpl/musicItemList.php", function(data) {
			var music;
			var elt='';
			for (var i = 0; i < musics.length; i++) {
				music = musics[i];
				elt = tplawesome(data, [{"videoId":music.videoId, "cover":music.cover, "title":music.title, "artist":music.artist, "artistId":music.artistId, "album":music.album, "albumId":music.albumId, "genre":music.genre }]);
				$('#fav-container').append(elt);
			};
		});
		// for (var i=0; i < musics.length ; i++) {
		// 	elt='<div data-video-id="'+musics[i]['videoId']+'"  draggable="true" >';
		// 	elt+='<span><img src="'+musics[i]['cover']+'"></span>';
		// 	elt+='<span>'+musics[i]['title']+'</span>';
		// 	elt+='<span><a href="?artist='+musics[i]['artistId']+'">'+musics[i]['artist']+'</a></span>';
		// 	elt+='<span><a href="?artist='+musics[i]['artistId']+'&album='+musics[i]['albumId']+'">'+musics[i]['album']+'</a></span>';
		// 	elt+='<span>'+musics[i]['genre']+'</span>';
		// 	elt+='</div>';
		// 	$('#fav-container').append(elt);
		// }
	}
}