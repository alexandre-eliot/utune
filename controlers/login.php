<?php

include_once('modele/login/check_connection.php');
$error_user;

if (isset($_POST['email']) and isset($_POST['password']) ){
	$email=htmlspecialchars($_POST['email']);
	$password=htmlspecialchars($_POST['password']);

	setcookie('email', $_POST['email'], time() + (86400 * 30), "/"); // 86400 = 1 day

	$result = check_connection($email, $password);
	if ($result!=false and $result!='' and (count($result) == 1)){
		$_SESSION['user'] = $result[0];
		echo("<script>window.location.assign('?page=home');</script>");
	}
	else{
		$error_user = true;
	}
}

?>