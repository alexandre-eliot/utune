<?php

include_once('modele/signup/register.php');
include_once('modele/signup/get_userInfos.php');
include_once('modele/signup/check_userInfos.php');

$errors;

if (isset($_POST['pseudo']) and isset($_POST['email']) and isset($_POST['password']) and isset($_POST['confirm_password']) ){

	$pseudo=htmlspecialchars($_POST['pseudo']);
	$email=htmlspecialchars($_POST['email']);
	$password=htmlspecialchars($_POST['password']);
	$confirm_password=htmlspecialchars($_POST['confirm_password']);

	$errors = check_userInfos($pseudo, $email, $password, $confirm_password);

	$empty = ($errors->email);
	$empty+= ($errors->pseudo);
	$empty+= ($errors->passwords);
	
	if ($empty<=0){
		register($pseudo, $email, $password);
		$_SESSION['user']=get_userInfos($pseudo, $email);
		echo("<script>window.location.assign('?page=home');</script>");
	}
}

?>