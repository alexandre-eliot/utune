<li class="music" data-video-id="{{videoid}}" draggable="true">
	<div>
		<figure class="play glyphicon glyphicon-play">
			<img src="{{cover}}" alt="Cover">
		</figure>
	</div>
	<div>
		<div>
			<div>
				<span class="music-title">{{title}}</span>
				<div>
					<span class="music-artist">{{artist}}</span>
				</div>
			</div>
			<a>
				<span class="link-w"></span>
				<div>
					<div class="addFav">
						<span class="heart-o"></span>
						<span>Add to favorites</span>
					</div>
					<div class="addLib">
						<span class="library"></span>
						<span>Add to library</span>
					</div>
				</div>
			</a>
		</div>
	</div>
</li>