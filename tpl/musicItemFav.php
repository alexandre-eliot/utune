<li class="music" data-video-id="{{videoid}}" draggable="true">
	<div>
		<figure class="play glyphicon glyphicon-play">
			<img src="{{cover}}" alt="Cover">
		</figure>
	</div>
	<div>
		<div>
			<div>
				<span class="music-title">{{title}}</span>
				<div>
					<span class="music-artist">{{artist}}</span>
				</div>
			</div>
			<a>
				<span class="link-w"></span>
				<div>
					<div class="removeFav">
						<span class="heart"></span>
						<span>Remove from favorites</span>
					</div>
					<div class="removeLib">
						<span class="library"></span>
						<span>Remove from library</span>
					</div>
				</div>
			</a>
		</div>
	</div>
</li>