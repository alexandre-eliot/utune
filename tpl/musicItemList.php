<div data-video-id="{{videoId}}" draggable="true" >
	<span><img src="{{cover}}"></span>
	<span>{{title}}</span>
	<span><a href="?artist={{artistId}}">{{artist}}</a></span>
	<span><a href="?artist={{artistId}}&album={{albumId}}">{{album}}</a></span>
	<span>{{genre}}</span>
</div>