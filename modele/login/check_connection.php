<?php

function check_connection($email, $password){
	global $bdd;
	$salt;

	$req = $bdd->prepare('SELECT salt 
		FROM users 
		WHERE email=:email');
	$req->bindParam(':email', $email);
	$req->execute();
	$res=$req->fetchAll();
	if ($req->rowCount() == 1) {
		$password_hash_salted=sha1($password.$res[0][0]);
		$req = $bdd->prepare('SELECT idUser, pseudo, email 
		FROM users 
		WHERE email=:email and password=:password');
		$req->bindParam(':email', $email);
		$req->bindParam(':password', ($password_hash_salted));
		$req->execute();
		$result = $req->fetchAll();
	} else {
		$result = false;
	}
	
	return ($result);
}

?>