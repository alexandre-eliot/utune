<?php
include_once('connexion_sql.php');

function unset_favorite($idUser, $videoId){

	global $bdd;
	$request = 'UPDATE musics
	SET favorite=0;
	WHERE users.idUser=:idUser and musics.videoId=:videoId';

	$req = $bdd->prepare($request);
	$req->bindParam(':idUser', $idUser);

	if($videoId!='null'){
		$req->bindParam(':videoId', $videoId);
	}

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function set_favorite($idUser, $videoId){

	global $bdd;
	$request = 'UPDATE musics
	SET favorite=1;
	WHERE users.idUser=:idUser and musics.videoId=:videoId';

	$req = $bdd->prepare($request);
	$req->bindParam(':idUser', $idUser);

	if($videoId!='null'){
		$req->bindParam(':videoId', $videoId);
	}

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

?>