<?php

include_once('connexion_sql.php');

function get_albums($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT albums.idAlbum as albumId, albums.name as album, albums.cover as cover, artists.idArtist as artistId , artists.name as artist
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album
		WHERE users.idUser=:idUser
		ORDER BY albums.name');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_albumsFav($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT albums.idAlbum as albumId, albums.name as album, albums.cover as cover
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album
		WHERE users.idUser=:idUser and musics.favorite="1"
		ORDER BY albums.name');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_albumsReqArtist($idUser, $artistId){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT artists.idArtist as artistId, artists.name as artist, albums.idAlbum as albumId, albums.name as album, albums.cover as cover
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album
		WHERE users.idUser=:idUser and artists.idArtist=:artistId');
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':artistId', $artistId);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

?>