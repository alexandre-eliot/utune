<?php
require_once('connexion_sql.php');

function get_favorites($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT musics.videoId as videoId, musics.title as title, artists.name as artist, artists.idArtist as artistId, albums.name as album, albums.idAlbum as albumId, albums.cover as cover, genres.name as genre
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON genres.idGenre=musics.genre
		WHERE users.idUser=:idUser and musics.favorite="1"
		ORDER BY musics.title ');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}
function get_favoritesReq($idUser, $filter='', $order='default'){

	global $bdd;
	$request = 'SELECT DISTINCT musics.videoId as videoId, musics.title as title, artists.name as artist, artists.idArtist as artistId, albums.name as album, albums.idAlbum as albumId, albums.cover as cover, genres.name as genre
	FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON genres.idGenre=musics.genre
	WHERE users.idUser=:idUser and musics.favorite="1" and ( musics.title LIKE "%":filter"%" or albums.name LIKE "%":filter"%" or artists.name LIKE "%":filter"%")';

	
	$request.='ORDER BY ';
	switch (strtolower($order)) {
		case 'title':
		$request.='musics.title';
		break;
		case 'artist':
		$request.='artists.name';
		break;
		case 'album':
		$request.='albums.name';
		break;
		case 'genre':
		$request.='genres.name';
		break;
		default:
		$request.='musics.title';
		break;
		
	}

	$req = $bdd->prepare($request);

	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':filter', $filter);

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

?>