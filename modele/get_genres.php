<?php
include_once('connexion_sql.php');

function get_genres($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT genres.name as genre
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON musics.genre=genres.idGenre 
		WHERE users.idUser=:idUser
		ORDER BY genres.name');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_genresFav($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT genres.name as genre
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON musics.genre=genres.idGenre 
		WHERE users.idUser=:idUser and musics.favorite="1"
		ORDER BY genres.name');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

?>