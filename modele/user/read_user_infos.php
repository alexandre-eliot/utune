<?php

function read_user_infos($id_user) {

	global $bdd;
	
	$req = $bdd->prepare(
		"SELECT pseudo, password, email, avatar, date_inscription, hash_validation
		FROM users
		WHERE id = :id_user"
		);

	$req->bindParam(':id_user', $id_user);
	$req->execute();
	
	if ($result = $req->fetch(PDO::FETCH_ASSOC)) {
	
		$req->closeCursor();
		return $result;
	}
	return false;
}
