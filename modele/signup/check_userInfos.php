<?php


function check_userInfos($pseudo, $email, $password, $confirm_password) {

	$errors = (object) array(
		'email' => (check_emailUse($email)),
		'pseudo' => (check_pseudoUse($pseudo)),
		'passwords' => (!check_passwords($password, $confirm_password))
	);
	return $errors;
}

function check_emailUse($email){
	global $bdd;
	$req = $bdd->prepare('SELECT idUser 
		FROM users 
		WHERE email=:email');
	$req->bindParam(':email', $email);
	$req->execute();
	$req->fetchAll();
	$result = ($req->rowCount() == 1);
	return ($result);
}
function check_pseudoUse($pseudo){
	global $bdd;
	$req = $bdd->prepare('SELECT idUser 
		FROM users 
		WHERE pseudo=:pseudo');
	$req->bindParam(':pseudo', $pseudo);
	$req->execute();
	$req->fetchAll();
	$result = ($req->rowCount() == 1);
	return ($result);
}
function check_passwords($password, $confirm_password){
	return $password === $confirm_password;
}

?>