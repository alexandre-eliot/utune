<?php

function validate_account_with_hash($hash_validation) {

	$pdo = PDO2::getInstance();

	$request = $pdo->prepare("UPDATE users SET
		hash_validation = ''
		WHERE
		hash_validation = :hash_validation");

	$request->bindValue(':hash_validation', $hash_validation);
	
	$request->execute();

	return ($request->rowCount() == 1);
}
