<?php

function get_userInfos($pseudo, $email){
	global $bdd;
	$req = $bdd->prepare('SELECT idUser, pseudo, email 
		FROM users 
		WHERE email=:email and pseudo=:pseudo');
	$req->bindParam(':email', $email);
	$req->bindParam(':pseudo', $pseudo);
	$req->execute();
	$result = $req->fetchAll();
	return $result[0];
}

?>