<?php

function register($pseudo, $email, $password) {
	global $bdd;
	$salt = uniqid(mt_rand(), true);
	$password_hash_salted=sha1($password.$salt);

	$req = $bdd->prepare('INSERT INTO users ( pseudo, email, timestamp, password, salt) 
		VALUES(:pseudo, :email, NOW(), :password_hash_salted, :salt)');
	$req->bindParam(':pseudo', $pseudo);
	$req->bindParam(':email', $email);
	$req->bindParam(':password_hash_salted', $password_hash_salted);
	$req->bindParam(':salt', $salt);
	$req->execute();	
}

?>