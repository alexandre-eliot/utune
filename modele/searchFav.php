<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}


require_once('get_favorites.php');

if (isset($_POST['filter']) and !is_null($_POST['filter'])){
	$filter = strip_tags(trim($_POST['filter']));
} else{
	$filter = '';
}

if (isset($_POST['order']) and !is_null($_POST['order'])){
	$order = strip_tags(trim($_POST['order']));
} else{
	$order = 'default';
}

$result=get_favoritesReq($_SESSION['user']['idUser'], $filter, $order);
echo json_encode($result);

?>