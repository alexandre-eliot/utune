<?php

include_once('connexion_sql.php');

function get_musics($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT musics.videoId as videoId, musics.title as title, artists.name as artist, albums.name as album, albums.cover as cover
		FROM (((users INNER JOIN artists ON users.idUser=artists.user) INNER JOIN albums ON artists.idArtist=albums.artist) INNER JOIN musics ON albums.idAlbum=musics.album)
		WHERE users.idUser=:idUser');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_musicsReqAlbum($idUser, $albumId){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT musics.videoId as videoId, musics.title as title, artists.name as artist, artists.idArtist as artistId, albums.name as album, albums.cover as cover
		FROM (((users INNER JOIN artists ON users.idUser=artists.user) INNER JOIN albums ON artists.idArtist=albums.artist) INNER JOIN musics ON albums.idAlbum=musics.album)
		WHERE users.idUser=:idUser and albums.idAlbum=:albumId');
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':albumId', $albumId);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_musicsReqArtist($idUser, $artistId){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT musics.videoId as videoId, musics.title as title, artists.name as artist, albums.idAlbum as albumId, albums.name as album, albums.cover as cover
		FROM (((users INNER JOIN artists ON users.idUser=artists.user) INNER JOIN albums ON artists.idArtist=albums.artist) INNER JOIN musics ON albums.idAlbum=musics.album)
		WHERE users.idUser=:idUser and artists.idArtist=:artistId');
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':artistId', $artistId);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_musicsReq($idUser, $title, $genre, $artistName, $albumName){

	global $bdd;
	$request = 'SELECT DISTINCT musics.videoId as videoId, musics.title as title, artists.name as artist, albums.name as album, albums.cover as cover
	FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON genres.idGenre=musics.genre
	WHERE users.idUser=:idUser';
	if ($title!=''){
		$request.=' and musics.title like "%":title"%"';
	}
	if($genre!='null'){
		$request.=' and genres.name=:genre';
	}
	if($artistName!='null'){
		$request.=' and artists.name=:artistName';
	}
	if($albumName!='null'){
		$request.=' and albums.name=:albumName';
	}

	$req = $bdd->prepare($request);
	$req->bindParam(':idUser', $idUser);
	if($title!=''){
		$req->bindParam(':title', $title);
	}
	if($genre!='null'){
		$req->bindParam(':genre', $genre);
	}
	if($artistName!='null'){
		$req->bindParam(':artistName', $artistName);
	}
	if($albumName!='null'){
		$req->bindParam(':albumName', $albumName);
	}

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

?>