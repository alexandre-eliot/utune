<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

include_once('change_favorite.php');

if (isset($_GET['videoId'])){
	$videoId = $_GET['videoId'];
	if ($videoId!='null'){
		$videoId = strip_tags(trim($videoId));
	}
} else {
	$videoId = 'null';
}

$result=set_favorite($_SESSION['user']['idUser'], $videoId);
echo json_encode($result);

?>