<?php
include_once('connexion_sql.php');

function rem_music($idUser, $videoId){
	NULL;
}

function add_music($idUser, $title, $genre, $videoId, $artist, $album){

	global $bdd;
	$added=false;
	
	$idArtist;
	$idAlbum;
	$idGenre;

	// check if artist with the same name exists if so then get its id
	$request = 'SELECT artists.idArtist as artist
	FROM users INNER JOIN artists ON users.idUser=artists.user
	WHERE users.idUser=:idUser and artists.name=:artist';
	$req = $bdd->prepare($request);
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':artist', $artist);

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();

	if(sizeof($res)){
		$idArtist = $res[0]['artist'];
	} else {
		$request = 'INSERT INTO artists ( name, user) 
		VALUES(:artist, :idUser)';

		$req = $bdd->prepare($request);
		$req->bindParam(':artist', $artist);
		$req->bindParam(':idUser', $idUser);

		if (!$req) {
			echo "\nPDO::errorInfo():\n";
			print_r($bdd->errorInfo());
		}
		$req->execute();
		$idArtist = $bdd->lastInsertId();
	}

	// check if album with the same name exists if so then get its id
	$request = 'SELECT albums.idArtist as album
	FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album 
	WHERE users.idUser=:idUser and artists.idArtist=:idArtist and albums.name=:album';
	$req = $bdd->prepare($request);
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':idArtist', $idArtist);
	$req->bindParam(':album', $album);

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();

	if(sizeof($res)){
		$idAlbum = $res[0]['album'];
	} else {
		$request = 'INSERT INTO albums ( name, artist) 
		VALUES(:album, :idArtist)';

		$req = $bdd->prepare($request);
		$req->bindParam(':album', $album);
		$req->bindParam(':idArtist', $idArtist);

		if (!$req) {
			echo "\nPDO::errorInfo():\n";
			print_r($bdd->errorInfo());
		}
		$req->execute();
		$idAlbum = $bdd->lastInsertId();
	}	

	// check if genre exists if so then get its id
	$request = 'SELECT genres.idGenre as genre
	FROM genres
	WHERE genres.name=:genre';
	$req = $bdd->prepare($request);
	$req->bindParam(':genre', $genre);

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();

	if(sizeof($res)){
		$idGenre = $res[0]['genre'];
	} else {
		$request = 'INSERT INTO genres ( name ) 
		VALUES(:genre)';

		$req = $bdd->prepare($request);
		$req->bindParam(':genre', $genre);

		if (!$req) {
			echo "\nPDO::errorInfo():\n";
			print_r($bdd->errorInfo());
		}
		$req->execute();
		$idGenre = $bdd->lastInsertId();
	}

	// check if music exists if so then get its id
	$request = 'SELECT musics.idMusic as music 
	FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album 
	WHERE users.idUser=:idUser and artists.idArtist=:idArtist and albums.idAlbum=:idAlbum';

	$req = $bdd->prepare($request);
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':idArtist', $idArtist);
	$req->bindParam(':idAlbum', $idAlbum);

	if (!$req) {
		echo "\nPDO::errorInfo():\n";
		print_r($bdd->errorInfo());
	}
	$req->execute();
	$res=$req->fetchAll();

	if(sizeof($res)){
		$added=false;
	} else {

		$request = 'INSERT INTO musics ( title, genre, videoId, artist, album) 
		VALUES(:title, :idGenre, :videoId, :idArtist, :idAlbum)';

		$req = $bdd->prepare($request);
		$req->bindParam(':title', $title);
		$req->bindParam(':idGenre', $idGenre);
		$req->bindParam(':videoId', $videoId);
		// $req->bindParam(':favorite', $favorite);
		$req->bindParam(':idArtist', $idArtist);
		$req->bindParam(':idAlbum', $idAlbum);

		if (!$req) {
			echo "\nPDO::errorInfo():\n";
			print_r($bdd->errorInfo());
		}
		$req->execute();

		$added=true;
	}

	return $added;
}

?>