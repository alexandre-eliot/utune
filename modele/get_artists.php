<?php
include_once('connexion_sql.php');

function get_artists($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT artists.name as artist, artists.idArtist as artistId
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON genres.idGenre=musics.genre 
		WHERE users.idUser=:idUser
		ORDER BY artists.name');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_artistsFav($idUser){

	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT artists.name as artist
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON musics.genre=genres.idGenre 
		WHERE users.idUser=:idUser and musics.favorite="1"
		ORDER BY artists.name');
	$req->bindParam(':idUser', $idUser);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

function get_genresReqArtist($idUser, $idArtist){
	global $bdd;
	$req = $bdd->prepare('SELECT DISTINCT genres.name as genre
		FROM users INNER JOIN artists ON users.idUser=artists.user INNER JOIN albums ON artists.idArtist=albums.artist INNER JOIN musics ON albums.idAlbum=musics.album INNER JOIN genres ON musics.genre=genres.idGenre 
		WHERE users.idUser=:idUser and artists.idArtist=:idArtist
		ORDER BY genres.name
		LIMIT 3');
	$req->bindParam(':idUser', $idUser);
	$req->bindParam(':idArtist', $idArtist);

	$req->execute();
	$res=$req->fetchAll();
	
	return ($res);
}

?>