<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}


include_once('get_musics.php');

if (isset($_GET['title'])){
	$title = $_GET['title'];
	if ($title!='null'){
		$title = strip_tags(trim($title));
	}
} else {
	$title = 'null';
}

if (isset($_GET['genre'])){
	$genre = $_GET['genre'];
	if ($genre!='null'){
		$genre = strip_tags(trim($genre));
	}
} else {
	$genre = 'null';
}

if (isset($_GET['artistName'])){
	$artistName = $_GET['artistName'];
	if ($artistName!='null'){
		$artistName = strip_tags(trim($artistName));
	}
} else {
	$artistName = 'null';
}

if (isset($_GET['albumName'])){
	$albumName = $_GET['albumName'];
	if ($albumName!='null'){
		$albumName = strip_tags(trim($albumName));
	}
} else {
	$albumName = 'null';
}

$result=get_musicsReq($_SESSION['user']['idUser'], $title, $genre, $artistName, $albumName);
echo json_encode($result);

?>