<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

$idUser = $_SESSION['user']['idUser'];

require_once('add_rem_music.php');

if(isset($_GET['title'])){
	$title = $_GET['title'];
	if ($title!=NULL){
		$title = strip_tags(trim($title));
	}
} else {
	$title = NULL;
}
if(isset($_GET['genre'])){
	$genre = $_GET['genre'];
	if ($genre!=NULL){
		$genre = strip_tags(trim($genre));
	}
} else {
	$genre = NULL;
}
if(isset($_GET['videoId'])){
	$videoId = $_GET['videoId'];
	if ($videoId!=NULL){
		$videoId = strip_tags(trim($videoId));
	}
} else {
	$videoId = NULL;
}
if(isset($_GET['artist'])){
	$artist = $_GET['artist'];
	if ($artist!=NULL){
		$artist = strip_tags(trim($artist));
	}
} else {
	$artist = NULL;
}
if(isset($_GET['album'])){
	$album = $_GET['album'];
	if ($album!=NULL){
		$album = strip_tags(trim($album));
	}
} else {
	$album = NULL;
}

$result=add_music($idUser, $title, $genre, $videoId, $artist, $album);
echo json_encode($result);

?>