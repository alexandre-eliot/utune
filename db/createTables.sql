CREATE TABLE `utune`.`users` (
	`idUser` INT NOT NULL AUTO_INCREMENT,
	`pseudo` VARCHAR(30) NOT NULL,
	`email` VARCHAR(254) NOT NULL,
	`timestamp` TIMESTAMP NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`salt` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`idUser`),
	UNIQUE(`pseudo`,`email`,`salt`)
)
ENGINE=InnoDB;
CREATE TABLE `utune`.`artists` (
	`idArtist` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(30) NOT NULL,
	`user` INT NOT NULL,
	PRIMARY KEY (`idArtist`),
	FOREIGN KEY (`user`) REFERENCES users(`idUser`) ON DELETE CASCADE
)
ENGINE=InnoDB;
CREATE TABLE `utune`.`playlists` (
	`idPlaylist` INT NOT NULL AUTO_INCREMENT,
	`user` INT NOT NULL,
	`name` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`idPlaylist`),
	FOREIGN KEY (`user`) REFERENCES users(`idUser`) ON DELETE CASCADE
)
ENGINE=InnoDB;
CREATE TABLE `utune`.`albums` (
	`idAlbum` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(30) NOT NULL,
	`artist` INT NOT NULL,
	`cover`	VARCHAR(30),
	PRIMARY KEY (`idAlbum`),
	FOREIGN KEY (`artist`) REFERENCES artists(`idArtist`) ON DELETE CASCADE
)
ENGINE=InnoDB;
CREATE TABLE `utune`.`genres` (
	`idGenre` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`idGenre`, `name`)
)
ENGINE=InnoDB;
CREATE TABLE `utune`.`musics` (
	`idMusic` INT NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(30) NOT NULL,
	`genre` INT NOT NULL,
	`videoId` VARCHAR(20) NOT NULL,
	`favorite` BOOLEAN NOT NULL DEFAULT 0,
	`artist` INT NOT NULL,
	`album` INT NOT NULL,
	PRIMARY KEY (`idMusic`),
	FOREIGN KEY (`genre`) REFERENCES genres(`idGenre`) ON DELETE CASCADE,
	FOREIGN KEY (`artist`) REFERENCES artists(`idArtist`) ON DELETE CASCADE,
	FOREIGN KEY (`album`) REFERENCES albums(`idAlbum`) ON DELETE CASCADE
)
ENGINE=InnoDB;
CREATE TABLE `utune`.`playlist` (
	`playlist` INT NOT NULL,
	`music` INT NOT NULL,
	PRIMARY KEY (`playlist`,`music`),
	FOREIGN KEY (`playlist`) REFERENCES playlists(`idPlaylist`) ON DELETE CASCADE,
	FOREIGN KEY (`music`) REFERENCES musics(`idMusic`) ON DELETE CASCADE
)
ENGINE=InnoDB;