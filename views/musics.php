<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/musics.css">
</head>
<div id="musics">
	<div  class="filter-form">
		<form>
			<div>
				<label for="filter-search">Search</label>
				<input id="filter-search" placeholder="Title">
			</div>
			<select name="genre">
				<option value="null" selected>Genre</option>
				<?php
				if(file_exists('modele/get_genres.php')){
					require_once('modele/get_genres.php');
				} else if(file_exists('../modele/get_genres.php')){
					require_once('../modele/get_genres.php');
				}
				
				$result=get_genres($_SESSION['user']['idUser']);
				for ($i=0; $i < count($result) ; $i++) {
					$elt='<option>'.$result[$i]['genre'].'</option>';
					echo($elt);
				}
				?>
			</select>
			<select name="artist">
				<option value="null" selected>Artist</option>
				<?php
				if(file_exists('modele/get_artists.php')){
					require_once('modele/get_artists.php');
				} else if(file_exists('../modele/get_artists.php')){
					require_once('../modele/get_artists.php');
				}
				
				$result=get_artists($_SESSION['user']['idUser']);
				for ($i=0; $i < count($result) ; $i++) {
					$elt='<option>'.$result[$i]['artist'].'</option>';
					echo($elt);
				}
				?>
			</select>
			<input type="submit" value="">
		</form>
	</div>
	<div class="search-wrapper">
		<div class="album-wrapper">
			<div class="filter-items">
			</div>
			<div class="albums">
				<?php
				if(file_exists('modele/get_albums.php')){
					require_once('modele/get_albums.php');
				} else if(file_exists('../modele/get_albums.php')){
					require_once('../modele/get_albums.php');
				}
				
				$result=get_albums($_SESSION['user']['idUser']);
				$fl='A';
				for ($i=0; $i < count($result) ; $i++) {
					if(!$result[$i]['album']) break;
					if($result[$i]['album'][0]!=$fl){
						$fl=$result[$i]['album'][0];
					}
					$elt='
					<div>
					<span>'.$fl.'</span>
					<div class="album" data-album="'.$result[$i]['album'].'">
					<img src="'.$result[$i]['cover'].'">
					<legend><a href="?page=albums&albumId='.$result[$i]['albumId'].'">'.$result[$i]['album'].'</a></legend>
					</div>
					</div>';
					echo($elt);
				}
				?>
			</div>
		</div>
		<div id="musics-container">
			<?php
			if(file_exists('modele/get_musics.php')){
				require_once('modele/get_musics.php');
			} else if(file_exists('../modele/get_musics.php')){
				require_once('../modele/get_musics.php');
			}
			
			$result=get_musics($_SESSION['user']['idUser']);
			for ($i=0; $i < count($result) ; $i++) {
				$elt='<li class="music" data-video-id="'.$result[$i]['videoId'].'" draggable="true">
				<div>
				<figure class="play glyphicon glyphicon-play">
				<img src="'.$result[$i]['cover'].'" alt="Cover">
				</figure>
				</div>
				<div>
				<div>
				<div>
				<span class="music-title">'.$result[$i]['title'].'</span>
				<div>
				<span class="music-artist">'.$result[$i]['artist'].'</span>
				</div>
				</div>
				<a>
				<span class="link-w"></span>
				<div>
				<div class="removeFav">
				<span class="heart"></span>
				<span>Remove from favorites</span>
				</div>
				</div>
				</a>
				</div>
				</div>
				</li>';
				echo($elt);
			}
			?>
		</div>
	</div>
</div>
<script src="js/searchMusic.js"></script>
</div>