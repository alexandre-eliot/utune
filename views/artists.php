<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

if(is_dir('modele/')){
	include_once('modele/get_artists.php');
	include_once('modele/get_albums.php');
} else if(is_dir('../modele/')){
	include_once('../modele/get_artists.php');
	include_once('../modele/get_albums.php');
}

$res='';

if(isset($_GET['artistId'])){


	$result = get_albumsReqArtist($_SESSION['user']['idUser'], $_GET['artistId']);
	if(sizeof($result)){
		$res.='<div id="artist-cover">
		<h2>'.$result[0]['artist'].'</h2>
		<div>';
		$genres = get_genresReqArtist($_SESSION['user']['idUser'], $_GET['artistId']);
		if(sizeof($genres)){
			for ($i=0; $i < count($genres) ; $i++) {
				$res.='<span>'.$genres[$i]['genre'].'</span>';
			}
		}
		$res.='</div>';
		$res.='</div>';
		$res.='<div id="album-wrapper">';
		for ($i=0; $i < count($result) ; $i++) {
			$res.='<div class="album">
			<img src="'.$result[0]['cover'].'">
			<a href="?album='.$result[0]['albumId'].'">'.$result[0]['album'].'</a>
			</div>';
		}
		$res.='</div>';
	} else {
		$res='No result found';
	}
}
else {

	$result=get_artists($_SESSION['user']['idUser']);
	if(sizeof($result)){
		$res.='<div id="artist-wrapper">';
		for ($i=0; $i < count($result) ; $i++) {
			$res.='<div class="artist">
			<img src="img/artist.png">
			<a href="?artist='.$result[$i]['artistId'].'">'.$result[$i]['artist'].'</a>
			</div>';
		}
		$res.='</div>';
	} else {
		$res='No result found';
	}
}

?>

<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/artists.css">
</head>
<div id="artists">
	<div id="artists-container">
		<?php

		echo($res);

		?>
	</div>
</div>