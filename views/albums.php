<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
if(is_dir('modele/')){
	require_once('modele/get_albums.php');
} else if(is_dir('../modele/')){
	require_once('../modele/get_albums.php');
}

$res='';

?>

<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/albums.css">
</head>
<div id="albums">
	<div  class="filter-form">
		<form>
			<div>
				<label for="filter-search">Search</label>
				<input id="filter-search" placeholder="Title">
			</div>
			<select name="genre">
				<option value="null" selected>Genre</option>
				<?php
				if(file_exists('modele/get_genres.php')){
					require_once('modele/get_genres.php');
				} else if(file_exists('../modele/get_genres.php')){
					require_once('../modele/get_genres.php');
				}
				
				$result=get_genres($_SESSION['user']['idUser']);
				for ($i=0; $i < count($result) ; $i++) {
					$elt='<option>'.$result[$i]['genre'].'</option>';
					echo($elt);
				}
				?>
			</select>
			<select name="artist">
				<option value="null" selected>Artist</option>
				<?php
				if(file_exists('modele/get_artists.php')){
					require_once('modele/get_artists.php');
				} else if(file_exists('../modele/get_artists.php')){
					require_once('../modele/get_artists.php');
				}
				
				$result=get_artists($_SESSION['user']['idUser']);
				for ($i=0; $i < count($result) ; $i++) {
					$elt='<option>'.$result[$i]['artist'].'</option>';
					echo($elt);
				}
				?>
			</select>
			<input type="submit" value="">
		</form>
	</div>
	<div id="album-container">
		<?php

		$result=get_albums($_SESSION['user']['idUser']);
		if(sizeof($result)){
			for ($i=0; $i < count($result) ; $i++) {
				$res.='	<li class="album">
				<img src="'.$result[$i]['cover'].'">
				<a  href="?artist='.$result[$i]['artistId'].'">'.$result[$i]['artist'].'</a>
				<a href="?artist='.$result[$i]['artistId'].'&album='.$result[$i]['albumId'].'">'.$result[$i]['album'].'</a>
				</li>';
			}
		} else {
			$res='No result found';
		}

		echo($res);

		?>
	</div>
</div>