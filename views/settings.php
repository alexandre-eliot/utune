<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="settings/view/style.css">
</head>
<div id="settings">
	<div id="cover-picture">
		<span class="settings-w"></span>
		<h1>Settings</h1>
	</div>
	<div id="settings-wrapper"></div>
</div>