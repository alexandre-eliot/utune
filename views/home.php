<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/404.css">
</head>
<div id="home">
	<div id="cover-picture">
		<span class="home-w"></span>
		<h1>Home</h1>
	</div>
	<div id="home-container">
	</div>
</div>