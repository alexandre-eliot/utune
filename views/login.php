<!DOCTYPE html>
<html>
<head>
	<title>Youtune - Login</title>
	<link type="text/css" rel="stylesheet" href="styles/pages/login.css"></link>
	<meta charset="UTF-8">
	<link rel="icon" href="img/logo.png">
</head>
<body>
	<div id="filter"></div>
	<img src="img/Utune-w.png" id="login-logo" alt="Youtune logo">
	<a href="?page=signup">Sign Up</a>
	<div id="login-form" name="login-form">
		<div id="login" class="content">
			<div>
				<span>Entrez votre email et mot de passe <strong>pour vous connecter</strong></span>
			</div>
			<form method="post" action="">
				<div>
					<label for="email">Email*</label>
					<input type="email" name="email" placeholder="Email" id="email" value="<?php echo( 
					isset($_COOKIE['email'])?$_COOKIE['email']:'' )  ?>" required>
				</div>
				<div>
					<div>
						<label for="password">Mot de passe*</label>
						<input type="password" name="password" placeholder="Mot de passe" id="password" required>
					</div>
					<span onclick="showPassword()">Show</span>
				</div>
				<?php 
					if(isset($error_user) and $error_user = true){
						echo("<p>Wrong email and password combination</p>");
					}
				?>
				<div>
					<div>
						<span>J'ai oublié mon mot de passe</span>
					</div>
					<div>
						<input type="submit" name="login-form" value="Log In">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
function showPassword(){
	var p = document.getElementById('password');
	if(p.type=="password"){
		p.type = 'text';
	}
	else{
		p.type = 'password';
	}
}
</script>
