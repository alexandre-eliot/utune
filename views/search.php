<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<div id="rechercher">
	<div id="cover-picture">
		<h1>Rechercher</h1>
		<img src="images/YouTune-full-2.png">
		<form id="search_form" action="#">
			<input id="search" placeholder="Rechercher..." required>
			<input type="image" src="images/menu/loupe.png" alt="Submit">
		</form>
	</div>
	<div id="search-results">
		<table>
			<tr>
				<th>#</th>
				<th></th>
				<th>Titre</th>
				<th></th>
			</tr>
		</table>
		<script type="text/javascript">musicSending();</script>
		<!-- scripts -->
		<script src="js/app.js"></script>
		<script src="https://apis.google.com/js/client.js?onload=init"></script>
	</div>
</div>