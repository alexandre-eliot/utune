<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/404.css">
</head>
<div id="404">
	<div id="cover-picture">
		<span class="404-w"></span>
		<h1>ERROR 404 :</h1>
	</div>
	<div id="404-container">
		<h3>Sorry the page you requested does not exist</h3>
	</div>
</div>