<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/add_music.css">
</head>
<div id="add_music">
	<div id="cover-picture">
		<span class="add-music-w"></span>
		<h1>Add a music</h1>
	</div>
	<div id="add_music-wrapper">
		<form>
			<div>
				<label for="cover">Cover</label>
				<img id="imgCover" src="img/logo-s-w.png" alt="cover">
				<input id="cover" name="cover" value="<?php if(isset($_POST['url_cover'])){ echo($_POST['url_cover']);} ?>" placeholder="url of the cover" />
				<script>
				$('#cover').on('keyup',function (){
					$('#imgCover')[0].src=$(this).val();
				});
				</script>
			</div>
			<div>
				<div>
					<label for="title">Title</label>
					<input id="title" name="title" value="<?php if(isset($_POST['title'])){ echo($_POST['title']);} ?>" placeholder="Title">
				</div>
				<div>
					<label for="genre">Genre</label>
					<input id="genre" name="genre" value="<?php if(isset($_POST['genre'])){ echo($_POST['genre']);} ?>" placeholder="Genre">
				</div>
				<div>
					<label for="video_id">Video Id</label>
					<input id="video_id" name="video_id" value="<?php if(isset($_POST['video_id'])){ echo($_POST['video_id']);} ?>" placeholder="Video Id" required>
				</div>
				<div>
					<label for="artist">Artist</label>
					<input id="artist" name="artist" value="<?php if(isset($_POST['artist'])){ echo($_POST['artist']);} ?>" placeholder="Artist">
				</div>
				<div>
					<label for="album">Album</label>
					<input id="album" name="album" value="<?php if(isset($_POST['album'])){ echo($_POST['album']);} ?>" placeholder="Album">
				</div>
			</div>
			<div>
				<input type="submit" name="add_music" value="Add">
			</div>
		</form>
		<script src="js/AlbumCover.js"></script>
		<script src="js/addMusic.js"></script>
		<script>
		$('#add_music-wrapper > form').on('submit', function () {
			var title = $('#title').val();
			var genre = $('#genre').val();
			var video_id = $('#video_id').val();
			var artist = $('#artist').val();
			var album = $('#album').val();
			addMusic(title, genre, video_id, artist, album);
			$(this).find("input:not[type='submit'], textarea").val("");
			return false;
		});
		</script>
	</div>
</div>