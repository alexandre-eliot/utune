<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/library.css">
</head>
<div id="library">
	<div id="cover-picture">
		<span class="library-w"></span>
		<h1>Library</h1>
	</div>
	<div id="nav">
		<a href="?page=library&subpage=musics">Musics</a>
		<a href="?page=library&subpage=albums">Albums</a>
		<a href="?page=library&subpage=artists">Artists</a>
	</div>
	<script>
	var a = $("#library #nav a");

	a.click( function() {
		history.pushState({ path: this.path }, '', this.href);
		a.each(function(i){
			a[i].removeAttribute("selected");
		});
		$(this).attr("selected","selected");

		var url = $(this)[0].href.replace(/.*?subpage=/,'');
		url = 'views/'+url+'.php';
		$.get(url, function(data) {
			$('#main-wrapper main #library #library-wrapper').html(data);    
		});
		return false;
	});
	</script>
	<div id="library-wrapper">
		<?php
			if(isset($_GET['subpage'])){
				include_once('views/'.$_GET['subpage'].'.php');
			}
		?>
	</div>
</div>