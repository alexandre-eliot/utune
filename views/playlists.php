<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/playlists.css">
</head>
<div id="playlists">
	<div id="cover-picture">
		<span class="playlist-w"></span>
		<h1>Playlists</h1>
	</div>
	<form class="filter-form">
	</form>
	<div id="playlist-wrapper">
			<div class="playlists">
				<label class="play glyphicon glyphicon-play"></label>
				<img src="img/instruments.png">
				<a>Playlist name</a>
			</div>
	</div>
</div>