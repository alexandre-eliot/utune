<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/history.css">
</head>
<div id="history">
	<div id="cover-picture">
		<span class="history-w"></span>
		<h1>History</h1>
	</div>
	<form class="filter-form">
		<select name="genre">
			<option value="Genre" selected>Genre</option>
			<option value="Electro">Electro</option>
			<option value="Jazz">Jazz</option>
			<option value="Musique classique">Musique classique</option>
			<option value="Opera">Opera</option>
			<option value="Pop">Pop</option>
			<option value="Reggae">Reggae</option>
			<option value="Rap">Rap</option>
			<option value="Rock">Rock</option>
			<option value="Soul">Soul</option>
			<option value="Variété internationale">Variété internationale</option>
			<option value="Variété française">Variété française</option>
		</select>
		<input placeholder="Filtre" required="">
		<input type="submit" value="Rechercher">
	</form>
	<div id="history-container">
	</div>
</div>