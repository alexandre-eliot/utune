<?php
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}

include_once('modele/get_favorites.php');
?>

<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="styles/pages/favorites.css">
</head>
<div id="favorites">
    <div id="cover-picture">
        <span class="heart-w"></span>
        <h1>Favorites</h1>
    </div>
    <div id="container">
        <div class="filter-form">
            <form>
                <span class="glyphicon glyphicon-play"></span>
                <span class="glyphicon glyphicon-random"></span>
                <div>
                    <label for="filter-search">Track</label>
                    <input id="filter-search" placeholder="Filter">
                </div>

                <span class="link-w"></span>
            </form>
        </div>
        <div id="filter-order">
            <span></span>
            <input type="radio" name="order" id="title" value="title" checked >
            <label for="title">Title</label>
            <input type="radio" name="order" id="artist" value="artist">
            <label for="artist">Artist</label>
            <input type="radio" name="order" id="album" value="album">
            <label for="album">Album</label>
            <input type="radio" name="order" id="genre" value="genre">
            <label for="genre">Genre</label>
        </div>
        <div id="fav-container">
            <?php
            $result=get_favorites($_SESSION['user']['idUser']);
            for ($i=0; $i < count($result) ; $i++) {
                $elt='<div data-video-id="'.$result[$i]['videoId'].'"  draggable="true" >';
                $elt.='<span><img src="'.$result[$i]['cover'].'"></span>';
                $elt.='<span>'.$result[$i]['title'].'</span>';
                $elt.='<span><a href="?artist='.$result[$i]['artistId'].'">'.$result[$i]['artist'].'</a></span>';
                $elt.='<span><a href="?artist='.$result[$i]['artistId'].'&album='.$result[$i]['albumId'].'">'.$result[$i]['album'].'</a></span>';
                $elt.='<span>'.$result[$i]['genre'].'</span>';
                $elt.='</div>';
                echo($elt);
            }
            ?>
        </div>
    </div>
</div>
<script src="js/searchFav.js"></script>