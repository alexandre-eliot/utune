<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

if(is_dir('modele/')){
	include_once('modele/get_artists.php');
	include_once('modele/get_albums.php');
	require_once('modele/get_musics.php');
} else if(is_dir('../modele/')){
	include_once('../modele/get_artists.php');
	include_once('../modele/get_albums.php');
	require_once('../modele/get_musics.php');
}

$res='';

if(isset($_GET['artist'])){

	if(isset($_GET['album'])){
		$result = get_musicsReqAlbum($_SESSION['user']['idUser'], $_GET['album']);
		if(sizeof($result)){
			$res.='<div id="container">';
			$res.='<div id="album-container">';
			$res.='<div>
			<figure><img src="'.$result[0]['cover'].'"></figure>
			<a href="?artist='.$result[0]['artistId'].'">'.$result[0]['artist'].'</a>
			<legend>'.$result[0]['album'].'</legend>
			</div>';
			$res.='<ul>';
			for ($i=0; $i < count($result) ; $i++) {
				$res.='	<li class="music" data-video-id="'.$result[$i]['videoId'].'">
				<span class="play glyphicon glyphicon-play"></span>
				<span class="nb"></span>
				<span class="title">'.$result[$i]['title'].'</span>
				<a>
				<span class="link-w"></span>
				<div>
				<div class="addFav">
				<span class="heart-o"></span>
				<span>Add to favorites</span>
				</div>
				<div class="addLib">
				<span class="library"></span>
				<span>Add to library</span>
				</div>
				</div>
				</a>
				</li>';
			}
			$res.='</ul>';
			$res.='</div>';
			$res.='</div>';
		} else {
			$res='No result found';
		}
	} else {

		$result = get_albumsReqArtist($_SESSION['user']['idUser'], $_GET['artist']);
		if(sizeof($result)){
			$res.='<div id="artist-cover">
			<h2>'.$result[0]['artist'].'</h2>
			<div>';
			$genres = get_genresReqArtist($_SESSION['user']['idUser'], $_GET['artist']);
			if(sizeof($genres)){
				for ($i=0; $i < count($genres) ; $i++) {
					$res.='<span>'.$genres[$i]['genre'].'</span>';
				}
			}
			$res.='</div>';
			$res.='</div>';
			$res.='<div id="album-wrapper">';
			for ($i=0; $i < count($result) ; $i++) {
				$res.='<div class="album">
				<img src="'.$result[0]['cover'].'">
				<a href="?artist='.$_GET['artist'].'&album='.$result[0]['albumId'].'">'.$result[0]['album'].'</a>
				</div>';
			}
			$res.='</div>';
		} else {
			$res='No result found';
		}
	}
}
else {
	echo("<script>window.location.assign('?page=library');</script>");
}

?>

<head>
	<link rel="stylesheet" type="text/css" href="styles/pages/artist.css">
</head>
<?php

echo($res);

?>