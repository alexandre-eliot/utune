<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<head>
	<link rel="stylesheet" type="text/css" href="my_profile/view/style.css">
</head>
<div id="my_profile">
	<div id="cover-picture">
		<span><img src="img/0dium.jpg"></span>
		<h1><?php echo($_SESSION['user']['pseudo']) ?></h1>
	</div>
	<div id="my_profile-wrapper"></div>
</div>