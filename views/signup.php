<!DOCTYPE html>
<html>
<head>
	<title>Youtune - Signup</title>
	<link type="text/css" rel="stylesheet" href="styles/pages/signup.css"></link>
	<meta charset="UTF-8">
	<link rel="icon" href="img/logo.png">
</head>
<body>
	<div id="filter"></div>
	<img src="img/Utune-w.png" id="login-logo" alt="Youtune logo">
	<a href="?page=login">Log In</a>
	<div id="login-form" name="login-form">
		<div id="signUp" class="content">
			<div>
				<span>Entrez vos informations personnelles <strong>pour créer un compte</strong></span>
			</div>
			<form action="" method="post">
				<input type="hidden" name="formID" value="users">
				<input type="hidden" name="redirect_to" value="youtune.html">
				<div>
					<label for="pseudo">Pseudo*</label>
					<input type="text" name="pseudo" placeholder="Pseudo" id="pseudo" required>
				</div>
				<?php if (isset($errors) and ($errors->pseudo)){echo("Pseudo already used");} ?>
				<div>
					<label for="email">Email*</label>
					<input type="email" name="email" placeholder="Email" id="email" required>
				</div>
				<?php if (isset($errors) and ($errors->email)){echo('Email already used');} ?>
				<div>
					<div>
						<label for="password">Mot de passe*</label>
						<input type="password" name="password" placeholder="Mot de passe" id="password" required>
					</div>
					<div>
						<label for="confirm_password">Répétez le mot de passe*</label>
						<input type="password" name="confirm_password" placeholder="Répétez le mot de passe" id="confirm_password" required>
					</div>
				</div>
				<?php if (isset($errors) and ($errors->passwords)){echo('Both passwords do not match');} ?>
				<div>
					<div>
						<span>En créant un compte vous agréez aux <a>Conditions d'utilisation et à la Politique de confidentialité</a></span>
					</div>
					<div>
						<input type="submit" name="login-form" value="Sign Up">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
var password = document.getElementById("password");
var confirm_password = document.getElementById("confirm_password");

function validatePassword(){
	if(password.value != confirm_password.value) {
		confirm_password.setCustomValidity("Le mot de passe doit être identique");
	} else {
		confirm_password.setCustomValidity('');
	}
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>