<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Utune</title>
	<link rel="icon" href="img/logo.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="styles/perso-bootstrap.css">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<link rel="stylesheet" type="text/css" href="styles/player.css">
	<link rel="stylesheet" type="text/css" href="styles/menu.css">
	<link rel="stylesheet" type="text/css" href="styles/aside.css">
	<link rel="stylesheet" type="text/css" href="styles/mainwrapper.css">
	<link rel="stylesheet" type="text/css" id="theme">
	<!-- Icons -->
	<link href="img/apple-touch-icon.png" rel="apple-touch-icon" />
	<link href="img/icons/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
	<link href="img/icons/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
	<link href="img/icons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/pusher.js"></script>
	<script src="js/changeFav.js"></script>
	<script src="js/searchResultsMove.js"></script>
	<script src="js/jquery.sortable.js"></script>
</head>
<body>
	<?php
	// if(isset($_GET['page'])){
	// 	echo("<script>
	// 		history.pushState('data', '', '?page=".$_GET['page']."');</script>");
	// }
	?>
	<div id="body-ctn">
		<header>
			<a href="?page=home">
				<div>
					<div></div>
					<div></div>
				</div>
			</a>
			<div id="search-bar" onclick="showSearchResBar()">
				<form id="search_form" action="">
					<input id="search" name="q" placeholder="Search">
					<input type="submit" value="" alt="Submit">
				</form>
			</div>
			<div id="nav-ctn">
				<nav>
					<a title="profile" href="?page=my_profile">
						<div><img src="img/0dium.jpg" class="profile"></div>
						<div><?php echo($_SESSION['user']['pseudo']) ?></div>
					</a>
					<a title="favorites" href="?page=favorites">
						<div class="heart-w"></div>
						<div>Favorites</div>
					</a>
					<a title="playlists" href="?page=playlists">
						<div class="playlist-w"></div>
						<div>Playlists</div>
					</a>
					<a title="library" href="?page=library&subpage=musics">
						<div class="library-w"></div>
						<div>Library</div>
					</a>
					<a title="add a music" href="?page=add_music">
						<div class="add-music-w"></div>
						<div>Add a music</div>
					</a>
					<a title="history" href="?page=history">
						<div class="history-w"></div>
						<div>History</div>
					</a>
					<a title="settings" href="?page=settings">
						<div class="settings-w"></div>
						<div>Settings</div>
					</a>
				</nav>
				<script src="js/menu.js"></script>
			</div>
			<div id="logout" title="Log out">
				<span class="logout-w"></span>
				<span>Log out</span>
			</div>
		</header>
		<div id="main-wrapper">
			<div id="search-results">
				<div>
					<script src="js/jquery.sortable.min.js"></script>
					<ul class="sortable">
					</ul>
				</div>
			</div>
			<main>
				<?php
				if(isset($_GET['artist'])){
					include_once('views/artist.php');
				}
				else if (isset($_GET['page'])){
					echo("<script>
						$('header a[href=\"?page=".$_GET['page']."\"]').attr('selected','selected');
						</script>");
					if(file_exists('views/'.$_GET['page'].'.php')){
						include_once('views/'.$_GET['page'].'.php');
					} else { 
						include_once('views/404.php');
					}
				}
				?>
			</main>
			<aside>
				<div>
					<h2>Mixer</h2>
					<div id="aside_buttons">
						<a href=""><div>Save as playlist</div></a>
						<a href=""><div>Clear</div></a>
						<a href=""><div class="save-playlist-w"></div></a>
						<a href=""><div class="clean-w"></div></a>
					</div>
				</div>
				<div id="file_attente">
					<ul class="sortable" id="playlist-elements">
						<li class="music" data-video-id="bpOSxM0rNPM" draggable="true">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">Do I Wanna Know?</span>
										<div>
											<span class="music-artist">Arctic Monkeys</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="removeFav">
												<span class="heart"></span>
												<span>Remove from favorites</span>
											</div>
											<div class="removeLib">
												<span class="library"></span>
												<span>Remove from library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
						<li class="music" data-video-id="Hrph2EW9VjY">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">Blablabla</span>
										<div>
											<span class="music-artist">Gigi D'Agostino</span>
											<span>&nbsp;-&nbsp;</span>
											<span class="music-album">L'Amour Toujours</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="addFav">
												<span class="heart-o"></span>
												<span>Add to favorites</span>
											</div>
											<div class="addLib">
												<span class="library"></span>
												<span>Add to library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
						<li class="music" data-video-id="0L8GehR_WzY">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">Miracle</span>
										<div>
											<span class="music-artist">Blackmill</span>
											<span>&nbsp;-&nbsp;</span>
											<span class="music-album">Miracle</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="addFav">
												<span class="heart-o"></span>
												<span>Add to favorites</span>
											</div>
											<div class="addLib">
												<span class="library"></span>
												<span>Add to library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
						<li class="music" data-video-id="Jbt8oH5Lxto">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">Love me Do</span>
										<div>
											<span class="music-artist">The Beatles</span>
											<span>&nbsp;-&nbsp;</span>
											<span class="music-album">Please Please Me</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="addFav">
												<span class="heart-o"></span>
												<span>Add to favorites</span>
											</div>
											<div class="addLib">
												<span class="library"></span>
												<span>Add to library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
						<li class="music" data-video-id="eWxyQlWolTI">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">The Golden Age</span>
										<div>
											<span class="music-artist">Woodkid</span>
											<span>&nbsp;-&nbsp;</span>
											<span class="music-album">The Golden Age</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="addFav">
												<span class="heart-o"></span>
												<span>Add to favorites</span>
											</div>
											<div class="addLib">
												<span class="library"></span>
												<span>Add to library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
						<li class="music" data-video-id="s8XIgR5OGJc">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">Don't Let Me Down ft. Daya</span>
										<div>
											<span class="music-artist">The Chainsmokers</span>
											<span>&nbsp;-&nbsp;</span>
											<span class="music-album">Unknown</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="addFav">
												<span class="heart-o"></span>
												<span>Add to favorites</span>
											</div>
											<div class="addLib">
												<span class="library"></span>
												<span>Add to library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
						<li class="music" data-video-id="GFQYaoiIFh8">
							<div>
								<figure class="play glyphicon glyphicon-play">
									<img src="img/Logo-s-w.png" alt="Cover">
								</figure>
							</div>
							<div>
								<div>
									<div>
										<span class="music-title">Demons</span>
										<div>
											<span class="music-artist">Imagine Dragons</span>
											<span>&nbsp;-&nbsp;</span>
											<span class="music-album">Unknown</span>
										</div>
									</div>
									<a>
										<span class="link-w"></span>
										<div>
											<div class="addFav">
												<span class="heart-o"></span>
												<span>Add to favorites</span>
											</div>
											<div class="addLib">
												<span class="library"></span>
												<span>Add to library</span>
											</div>
										</div>
									</a>
								</div>
							</div>
						</li>
					</ul>
					<script>
						$('.sortable').sortable({items : 'li'});
						$(".sortable").sortable({connectWith: ".connected"});
					</script>
				</div>
			</aside>
		</div>
	</div>
	<footer>
		<div id="player"></div>
		<div id="blur">
			<div></div>
		</div>
		<div id="infooter">

			<div id="column_1">
				<img src="img/Logo-s-w.png" title="music cover">
			</div>
			<div id="column_2">
				<span class="link-w" title="options"></span>
				<span class="heart-w-o" title="add to favorites"></span>
			</div>
			<div id="column_3">
				<div id="info">
					<span id="playerTitle">Title</span>
					<br>
					<span id="playerArtist">Artist</span>
					<span>&nbsp;-&nbsp;</span>
					<span id="playerAlbum">Album</span>
					<div id="time">
						<span class="time-elapsed">0:00</span>
						<span class="video-duration">/ 0:00</span>
					</div>
				</div>
				<div class="slider" title="Set time">
					<div class="sliderBar">
						<span class="sliderDragger"></span>
					</div>
					<span class="chargingBar"></span>
				</div>
			</div>
			<div id="column_4">
				<div id="buttons">
					<span class="button-loop glyphicon glyphicon-refresh" title="set loop"></span>
					<span class="button-prev glyphicon glyphicon-step-backward" title="previous"></span>
					<!-- button play/pause -->
					<span class="button-play glyphicon glyphicon-play" title="play"></span>
					<span class="button-pause glyphicon glyphicon-pause" title="pause" style="display:none"></span>

					<span class="button-next glyphicon glyphicon-step-forward" title="next"></span>
					<span class="button-rand glyphicon glyphicon-random" title="random"></span>
				</div>
				<div class="volume" title="Set volume">
					<div class="volumeBar">
						<span class="volumeDragger">
						</span>
					</div>
				</div>
			</div>
			<div id="column_5">
				<span class="button-cue glyphicon glyphicon-indent-right active" title="hide/show mixer"></span>
				<!-- button fullscreen -->
				<span class="button-resize glyphicon glyphicon-fullscreen" id="fullsize" title="fullscreen"></span>
			</div>
		</div>
		<script src="js/player.js"></script>
	</footer>
	<!-- scripts for youtube search -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/app.js"></script>
	<script src="https://apis.google.com/js/client.js?onload=init"></script>
</body>
</html>
