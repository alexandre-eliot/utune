<?php

session_start();

require_once('modele/connexion_sql.php');
require_once('conf/config.php');

if(isset($_POST['logout']) && $_POST['logout']){
	if(session_id() == '') {
		session_start();
	}
	session_unset();
	session_destroy();
}

$pageLoaded=false;

if(isset($_GET['page'])){
	switch (strtolower($_GET['page'])) {
		case 'login':
		case 'signup':
		include_once('controlers/'.strtolower($_GET['page']).'.php');
		include_once('views/'.strtolower($_GET['page']).'.php');
		$pageLoaded=true;
		break;

		default:
		$pageLoaded=false;
		break;
	}
}
if(!$pageLoaded){
	if(!isset($_SESSION['user'])) {
		echo("<script>window.location.assign('?page=login');</script>");
	} else {
		if(isset($_GET['page']) or isset($_GET['artist'])){
			include_once('views/main.php');
		} else {
			echo("<script>window.location.assign('?page=home');</script>");
		}
	}
}


?>